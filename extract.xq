declare variable $userPath as xs:string external;

let $files := doc('xmpp_files.xml')//xmpp
return 
<results>
{
	let $users := doc($userPath)//user
	for $user in $users
		return 
		<conversation id="{$user/@id}">
		{$user}
			<user_history>
			{
				for $path in $files
					let $xmpp := doc($path)
					let $history := $xmpp//user_history[@user = $user/@id]/*
					return $history
			}
			</user_history>
		</conversation>
}	
</results>
