<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:variable name="hexRef" select="document('fun_xmpp.xml')"/>

<xsl:template match="/">
    <results>
        <xsl:for-each select="//conversation">
            <conversation>
                <xsl:attribute name="id" select="./@id"/>
                <xsl:copy-of select="./user"/>
                <xsl:call-template name="replace">
                    <xsl:with-param name="source" select="./user_history" />
                </xsl:call-template>
            </conversation>
        </xsl:for-each>
    </results>
</xsl:template> 


<xsl:template name="replace">
    <xsl:param name="source" />
    <xsl:variable name="tagName" select="$source/name()"/>
    <xsl:variable name="encTagName" select="$hexRef//map[@key = $tagName]"/>
    <xsl:variable name="finalTagName">
        <xsl:choose>
            <xsl:when test="$encTagName != ''">
                <xsl:value-of select="$encTagName"/>                
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$tagName"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <xsl:element name="{$finalTagName}">
        <xsl:for-each select="$source/@*">
            <xsl:variable name="attName" select="name()"/>
            <xsl:variable name="encName" select="$hexRef//map[@key = $attName]"/>
            <xsl:attribute name="{$encName}">
                <xsl:value-of select="."/>
            </xsl:attribute>
            
        </xsl:for-each>
        <xsl:value-of select="text()"/>
        <xsl:for-each select="$source/child::*"> 
            
                <xsl:call-template name="replace">
                    <xsl:with-param name="source" select="." />
                </xsl:call-template>
            
        </xsl:for-each>
        
    </xsl:element>
    


</xsl:template>


</xsl:stylesheet>